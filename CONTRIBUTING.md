# Contributing Files to this Mirror

*Note: Any files contributed to this mirror must be redistributable. This means that their license must be in the REDISTRIBUTABLE license group, found in the `profiles/license_groups.yaml` file in the [fallout-4 repository](https://gitlab.com/portmod/fallout-4/-/blob/master/profiles/license_groups.yaml) (or likely, the [meta repository](https://gitlab.com/portmod/meta/-/blob/master/profiles/license_groups.yaml)*

You will need git-lfs installed.

Start by forking this repository, and creating a new branch (named appropriately to the file to be added).

If the file type is not already listed in `.gitattributes`, you should add it so that the file (and other files like it) get added as LFS files.

After committing the files, create a merge request.
